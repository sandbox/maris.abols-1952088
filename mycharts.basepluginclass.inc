<?php
/**
 * @file
 * This is file for abstract charting class
 */

/**
 * Abstract of charting class
 *
 * You will need to extend this class to implement
 * @TODO - need to rename it to simple class, if abstract methods will not be
 */
abstract class MyChartsBase {
  protected $series;
  protected $target = "";
  protected $settings = array();
  protected $pathToModule = "";

  /**
   * Function to set chart data.
   */
  abstract public function setChartData();

  /**
   * Function to define path to charting engine library.
   */
  abstract public function getPathToJs();

  /**
   * Function usual construct.
   */
  public function __construct($path_to_module, $chart_type) {
    $this->pathToModule = $path_to_module;
    $this->chart_type = $chart_type;
  }

  /**
   * Function to get protected settings value.
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   * Function to set settings.
   */
  public function setSettings($settings) {
    $this->settings = $settings;
  }

  /**
   * Function to load init javascript.
   */
  public function getLoadJs() {
    return array();
  }

  /**
   * Function to set usual data array.
   */
  public function setDataArray($arr) {
    $this->data_array = $arr;
  }

  /**
   * Function to set chart target wrapper.
   */
  public function setChartWrapper($id) {
    return NULL;
  }

  /**
   * Function to set chart axis title.
   */
  public function setAxisTitle() {
    return NULL;
  }

  /**
   * Function to set X axis title.
   */
  public function setXAxisTitle($title) {
    return NULL;
  }

  /**
   * Function to set data array for chart engine AND call function to set data.
   */
  public function setData() {
    if (empty($this->data_array)) {
      return NULL;
    }

    // Call chart data set.
    $this->setChartData();
  }
}
