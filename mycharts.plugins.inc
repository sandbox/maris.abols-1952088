<?php
/**
 * @file
 * File for plugins functions definition
 */

define('MYCHARTS_ENGINE_CONFIGFILE', 'config');

/**
 * Function to get mycharts ctools plugins.
 */
function _mycharts_plugins_get($id = NULL) {
  ctools_include('plugins');
  if (empty($id)) {
    return ctools_get_plugins('mycharts', 'mychart');
  }
  else {
    // No need for static or cache, as ctools does that for us ?.
    return ctools_get_plugins('mycharts', 'mychart', $id);
  }
}

/**
 * Function to get an array of yml files from a path.
 */
function _mycharts_path_config_list($path) {
  $charts = drupal_static(__FUNCTION__, array());
  if (!isset($charts[$path])) {
    $charts[$path] = file_scan_directory($path, '/\.yml$/',
        array(
          'nomask' => '/^' . MYCHARTS_ENGINE_CONFIGFILE
          . '\.yml$/','key' => 'name'));
  }
  return $charts[$path];
}

/**
 * Function to load and parse a YAML file.
 */
function _mycharts_load_yaml($path) {
  if (file_exists($path)) {
    $library_path = libraries_get_path("spyc");
    if (!class_exists('Spyc')) {
      require_once $library_path . "/Spyc.php";
    }

    $source = Spyc::YAMLLoad($path);

    return $source;
  }
}

/**
 * Function to load all charting engines list.
 */
function mycharts_engine_list() {
  return _mycharts_plugins_get();
}

/**
 * Function to load particular charting engines list.
 */
function mycharts_engine_getOptions($engine) {
  return _mycharts_plugins_get($engine);
}

/**
 * Function to load particular charting engine config file.
 */
function mycharts_engine_getConfig($engine) {
  $engine = mycharts_engine_getOptions($engine);
  if (
    $engine
    && is_array($engine)
    && isset($engine['path'])
  ) {
    return _mycharts_load_yaml($engine['path']
        . '/' . MYCHARTS_ENGINE_CONFIGFILE . '.yml');
  }
}

/**
 * Function to load particular charting engine chart types list.
 */
function mycharts_chart_list($engine) {
  $engine = mycharts_engine_getOptions($engine);
  if (
    $engine
    && is_array($engine)
    && isset($engine['path'])
  ) {
    return _mycharts_path_config_list($engine['path']);
  }
}

/**
 * Function to load particular charting engine chart config file.
 */
function mycharts_chart_getConfig($engine, $chart) {
  $engine = mycharts_engine_getOptions($engine);
  if (
    $engine
    && is_array($engine)
    && isset($engine['path'])
  ) {
    return _mycharts_load_yaml($engine['path']
        . '/' . trim(strtolower($chart)) . '.yml');
  }
}

/**
 * Function to retrieve an instance of a particular engine and chart type.
 */
function mycharts_plugin_load($engine, $type) {
  ctools_include('plugins');
  $plugin = ctools_get_plugins('mycharts', 'mychart', $engine);

  if (!empty($engine) && $plugin) {
    $class = $plugin['class'];

    // Include plugins class file.
    ctools_include(strtolower($plugin['name']), 'mycharts', 'plugins/'
        . strtolower($plugin['module']) . '/' . strtolower($plugin['title']));

    if ($class && is_array($plugin) && class_exists($class)) {
      return new $class($plugin['path'], $type);
    }
  }
}

/**
 * Function to retrieve plugin path.
 */
function mycharts_get_plugin($engine) {
  ctools_include('plugins');
  $plugin = ctools_get_plugins('mycharts', 'mychart', $engine);
  if ($plugin) {
    return $plugin;
  }

  return NULL;
}

/**
 * MyCharts Ctools plugins etc
 */

/**
 * Implements hook_ctools_plugin_type().
 */
function mycharts_ctools_plugin_type() {
  return array(
    'mychart' => array(
      'cache' => TRUE,
      'classes' => array(),
      'defaults' => array(),
      'alterable' => TRUE,
      'load themes' => FALSE,
    ),
  );
}

/**
 * Implements hook_ctools_plugin_directory().
 *
 * Here we indicated that we have plugins for mycharts.
 */
function mycharts_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'mycharts') {
    return 'plugins/mycharts';
  }
}
