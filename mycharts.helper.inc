<?php
/**
 * @file
 * File for all system/helper functions.
 */

define('CONFIG_TARGET_DOM', 'charts');

/**
 * Function to convert object to array.
 * 
 * @param object $data
 *   array structure that came from json_decode.
 * 
 * @return array
 *   data structure formatted to array.
 */
function mycharts_object_to_array($data) {
  if (is_array($data) || is_object($data)) {
    $result = array();
    foreach ($data as $key => $value) {
      $result[$key] = mycharts_object_to_array($value);
    }
    return $result;
  }
  return $data;
}

/**
 * Function to refactor select options array.
 * 
 * @param array $options
 *   [0] => 'Value1', [1] => 'Value2'.
 * 
 * @return array
 *   formatted ad ['Value1'] => 'Value1', ['Value2'] => 'Value2'.
 */
function mycharts_build_config_select_values($options) {
  $ret_options = array();
  if (is_array($options)) {
    foreach ($options as $value) {
      if (is_string($value)) {
        $ret_options[$value] = $value;
      }
    }
  }

  return $ret_options;
}

/**
 * Function to check boolean type options.
 * 
 * @param array $arr
 *   any kind of options for chart config form.
 * 
 * @return bool
 *   if options have 'true' or 'false', return true.
 */
function mycharts_is_only_boolean_labels($arr) {
  // @TODO - this is too dirty way to figure this out.
  if (!empty($arr)) {
    foreach ($arr as $v) {
      if ($v != 'true' && $v != 'false') {
        return FALSE;
      }
    }
  }

  return TRUE;
}

/**
 * Function to change integer values in array to actual integer case.
 * 
 * Some charting engine do not support integer values, if they are given 
 * into array as string. Here we are casting for example "11" to 11, or "0" to 0
 * 
 * @param array $arr
 *   data from config table to be casted as int.
 * 
 * @return array
 *   give value back by reference casted to float.
 */
function mycharts_array_values_to_int(&$arr) {
  if (empty($arr)) {
    return;
  }

  foreach ($arr as &$value) {
    if (is_numeric($value)) {
      $value = (float) $value;
    }

    // Go deeper.
    if (is_array($value) || is_object($value)) {
      mycharts_array_values_to_int($value);
    }
  }
}

/**
 * Function to load chart presets.
 * 
 * @param string $object_type
 *   'node' or 'view'.
 * @param string $object_id
 *   node_id or view_id.
 * 
 * @return object
 *   entity of presets for particular object.
 */
function mycharts_load_chart_preset($object_type, $object_id) {
  if (!$object_id) {
    return array();
  }

  $ids = FALSE;
  $conditions = array("object_type" => $object_type, "object_id" => $object_id);
  $reset = FALSE;

  // Load existing entity.
  $entity = entity_load('mycharts_preset', $ids, $conditions, $reset);

  return $entity;
}

/**
 * Function to save chart presets.
 * 
 * @param string $object_title
 *   just internal-use title for presets.
 * @param string $object_type
 *   'node' or 'view'.
 * @param string $object_id
 *   node_id or view_id.
 * @param string $graph_engine
 *   class name, like 'mycharts.googlecharts.class'
 * @param string $graph_type
 *   chart type, like 'piechart'.
 * @param string $config
 *   json_encoded array.
 * @param string $field_presets
 *   ?
 */
function mycharts_save_chart_preset($object_title, $object_type, $object_id,
    $graph_engine, $graph_type, $config, $field_presets = '') {
  module_load_include('inc', 'mycharts', 'mycharts.helper');
  // Get postponed news entry, if any.
  $entity = mycharts_load_chart_preset($object_type, $object_id);
  $config = (array) json_decode($config);

  // Some charting engine do not support integer values,
  // if they are given into array as string.
  // Here we are casting for example "11" to 11, or "0" to 0
  mycharts_array_values_to_int($config);

  $config = json_encode($config);

  $entry_obj
    = (object) array(
      'object_title' => $object_title,
      'object_type' => $object_type,
      'object_id' => $object_id,
      'graph_engine' => $graph_engine,
      'graph_type' => $graph_type,
      'config' => $config,
      'field_presets' => $field_presets,
    );

  if (!empty($entity) && is_array($entity)) {
    $first_elem = array_shift($entity);

    $entry_obj->pid = $first_elem->pid;
  }

  // Save chart config.
  entity_save('mycharts_preset', $entry_obj);
}
