Description
-----------

This module provides platform to implement any charting engine and use them
in Drupal. Module by default provide two charting libraries Google Charts and
Highcharts.

Installation
------------

Basic part of MyCharts module is installed automatically(create database table,
content type etc.), except Spyc library that you need to download and copy into
site/all/libraries directory. Please go to site
http://github.com/mustangostang/spyc/. Path site/all/libraries/spyc/Spyc.php
need to be accessible. Otherwise module will not work, because MyCharts module
use it for YAML configuration read.

Using MyCharts in Nodes
-----------------------

After installation MyCharts provide node content type called Charts. Fill in
'Title' and upload CSV file for Graph Data. For demo purposes MyCharts module
comes with 2 CSV files located in examples directory. Only charts content type
nodes have one extra tab "Charts" in edit mode and this tab contains all
charting engine defined properties and options, including selectboxes with
'Chart engines' and 'Charts types'. After selecting 'Chart engine' and 
'Chart type', please click 'Save' button. Only after that you should be able to
see configuration table. Below all configurations should be graph
preview. If not, try to watch Javascript error console and figure out which
graph property is not defined properly.

Using MyCharts in Views
-----------------------

If you want to generate chart using data from views result, just choose display
format 'MyCharts', click 'Apply'. Open 'Settings' page for particular display
format and choose charting engine and type and click 'Apply'. After that click
one more time on 'Settings' link and you will get all properties and options
that are defined for current charting engine. This process is little bit clumsy
but this is Views formatter specific, that it is impossible to load such big
form into Format window with AJAX. That's why I left it working in Drupal way.

MyCharts with Highcharts
------------------------

First of all you need to know that HightCharts is not free to use, so you need
to read http://shop.highsoft.com/highcharts.html if you want to use HightCharts
in commercial purposes. The reason I wrote implementation in MyCharts for
Highcharts that it is really cool and powerful charting engine with many many
features that are left unimplemented.

But if you want to use HighCharts, you need to download HighCharts library from
http://www.highcharts.com/download and put it in sites/all/libraries and in
result of that system should be accessible path
sites/all/libraries/highcharts/js/highcharts.js.

NOTE: Highcharts is tested with v3.0.4 and v4.0.1. Above and below of these
I cannot guarantee that everything will work forever and also worked in past.

In directory plugins/mycharts/highcharts is located one file called all.yml and
other file config.yml. First is for all chart types and other is for whole
charting engine, but it is not completely working in this stage.

MyCharts with Google Charts
---------------------------

Google charts support is already built in basic MyCharts installation and all
Google Charts libraries are also included from web. In directory
plugins/mycharts/google charts/ is located all .yml files which for
Google Charts are multiple not just one, because every charting type have
specific parameters which are not working in other charting types etc. File
named config.yml is for basic configuration for whole charting engine, but it
is not completely working in this stage.

Embed MyChart nodes
-------------------

MyCharts nodes is tested successfully to be embed using Node Embed module. For
more information look into: https://drupal.org/project/node_embed

Implementing new charting engine
--------------------------------

@ TODO - will write detailed documentation later.. ;)
