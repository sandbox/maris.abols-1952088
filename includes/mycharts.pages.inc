<?php
/**
 * @file
 * This settings table theme function for view FORMATTER MyCharts.
 */

/**
 * Output the table of field settings within the charts settings form.
 */
function theme_mycharts_settings_fields($variables) {
  $element = $variables['element'];

  $table = array(
    'sticky' => FALSE,
  );
  $table['header'] = array(
    array('data' => t('Field name')),
    array(
      'data' => t('Provides labels'),
      'class' => array('chart-label-field', 'checkbox')),
    array(
      'data' => t('Provides data'),
      'class' => array('chart-data-field', 'checkbox')),
  );
  foreach ($element['label_field']['#options'] as $field_name => $field_label) {
    $element['label_field'][$field_name]['#title_display'] = 'attribute';
    $element['data_fields'][$field_name]['#title_display'] = 'attribute';

    $row = array();
    $row[] = array(
      'data' => $field_label,
    );
    $row[] = array(
      'data' => drupal_render($element['label_field'][$field_name]),
      'class' => array('chart-label-field', 'checkbox'),
    );
    $row[] = array(
      'data' => $field_name ? drupal_render($element['data_fields'][$field_name]) : '',
      'class' => array('chart-data-field', 'checkbox'),
    );
    $table['rows'][] = $row;
  }

  $wrapper = array(
    '#title' => t('Chart fields'),
    '#id' => 'chart-fields',
    '#theme_wrappers' => array('form_element'),
    '#markup' => theme('table', $table),
    '#description' => t('Any field may be used to provide labels.'),
  );

  return $wrapper;
}
