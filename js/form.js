(function ($) {
  function update_dropdown(wrapper, optionsJSON) {
    var data = JSON.parse(optionsJSON);
    // you might want to empty it first with .empty()
    var $select = $('#' + wrapper);

    var chosen = $select.val();
    $select.empty();

    for (var i in data) {
      var o = $('<option/>', {value: data[i].toLowerCase()}).text(data[i]);

      if (chosen == data[i]) {
        o.attr("selected", true);
      }
      o.appendTo($select);
    }
  }

  function loadTypes(data) {
    console.log(data);
    $('.ajax-progress-throbber').hide();
    update_dropdown('type-wrapper', data);
  }

  Drupal.behaviors.myChartsAdmin = {
    attach: function(context, settings) {
      $('#engine-wrapper', context).change(function() {
        if ($(this).val()) {
          $.ajax({
            type: "POST",
            url: '/ajax_load/engine_types/' + $(this).val(),
            dataType: "html",
            beforeSend: function() {
              $('#engine-wrapper', context).parent().once().append($('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>', context));
              $('.ajax-progress-throbber', context).show();
            },
            success: loadTypes,
          });
        }
      });
    }
  };
})(jQuery);
