(function ($) {
  Drupal.behaviors.mycharts = {
    attach: function(context, settings) {
       $('.simple-colorpicker', context).ColorPicker({
        onSubmit: function(hsb, hex, rgb, el) {
          $(el).val('#' + hex);
          $(el).ColorPickerHide();
          $(el).trigger('focusout');
        },
        onBeforeShow: function () {
          $(this).ColorPickerSetColor(this.value);
        }
      })
      .bind('keyup', function(){
        $(this).ColorPickerSetColor(this.value);
      });
    }
  }
})(jQuery);
