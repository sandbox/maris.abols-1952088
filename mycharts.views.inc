<?php
/**
 * @file
 * MyCharts Views Integrations
 */

/**
 * Implements hook_views_plugins().
 */
function mycharts_views_plugins() {
  return array(
    'style' => array(
      'mycharts_chart' => array(
        'title' => t('MyCharts'),
        'help' => t('Displays rows as a Chart using MyCharts.'),
        'path' => drupal_get_path('module', 'mycharts') . '/plugins/views',
        'handler' => 'MychartsViewsPluginStyleMycharts',
        'theme' => 'mycharts_view_mycharts',
        'uses fields' => TRUE,
        'uses row plugin' => FALSE,
        'uses row class' => FALSE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}

/**
 * Theme function for the views style handler.
 *
 * This will just format an element and hand it to the normal 
 * mycharts theme function.
 */
function theme_mycharts_view_mycharts($view) {
  module_load_include('inc', 'mycharts', 'mycharts.helper');

  // Load chart data for this view.
  $presets = mycharts_load_chart_preset('view', $view['view']->vid);

  if (isset($presets) && !empty($presets)) {
    $first = array_shift($presets);

    $chart_engine = $first->graph_engine;
    $chart_type = $first->graph_type;
    $saved_config = mycharts_object_to_array(json_decode($first->config));

    if ($first->field_presets) {
      $field_presets = json_decode($first->field_presets);

      if (!empty($field_presets)) {
        $label_field = $field_presets->label_field;
      }
    }
  }

  $view_data = array();
  if (!empty($view['view']->result)) {
    foreach ($view['view']->result as $result_index => $result_value) {
      if (!empty($view['view']->field)) {
        $cnt = 1;
        foreach ($view['view']->field as $field_key => $field) {
          $data = $field->get_value($result_value);

          // If value is array, try to access value - only one
          // field value is supported.
          if (is_array($data)) {
            $first_data = array_shift($data);
            if ($first_data['value']) {
              $data = $first_data['value'];
            }
          }

          if ($field_key != $label_field) {
            $view_data[$result_index][$cnt++] = $data;
          }
          else {
            $view_data[$result_index][0] = $data;
          }
        }
      }
    }
  }

  // Figure out all field labels - from default display.
  $first_row = array();
  if (isset($view['view']->display['default']->display_options['fields'])) {
    $first_row = array();
    $fields = $view['view']->display['default']->display_options['fields'];

    foreach ($fields as $field) {
      $first_row[]
        = $field['label'] ? $field['label'] : ucfirst($field['field']);
    }
  }

  $data = array(
    '#labels' => $first_row,
    '#rows' => $view_data,
  );

  // Chart preview demo.
  $element = array(
    '#data' => $data,
    '#id' => 'graph-preview',
    '#settings' => $saved_config,
    '#engine' => $chart_engine,
    '#type' => $chart_type,
  );

  return theme('mychart', $element);
}
