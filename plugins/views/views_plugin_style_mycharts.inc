<?php
/**
 * @file
 * Contains the grid style plugin.
 */

/**
 * Style plugin to render each item in a grid cell.
 */
class MychartsViewsPluginStyleMycharts extends views_plugin_style {
  /**
   * Set default options.
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['config_wrapper']['interactive']['choose'] = array(
      'default' => array(
        'engine' => '',
        'type' => '',
      ),
    );
    $options['config_wrapper']['interactive']['config'] = array(
      'default' => array(),
    );

    return $options;
  }

  /**
   * Render the given style.
   */
  public function options_form(&$form, &$form_state) {
    module_load_include('inc', 'mycharts', 'mycharts.forms');

    parent::options_form($form, $form_state);

    // Load saved data for current object.
    $presets = mycharts_load_chart_preset('view', $this->view->vid);

    $engine = "";
    $type = "";
    $label_field = "";
    $data_fields = "";
    if (isset($presets) && !empty($presets)) {
      $first = array_shift($presets);

      $engine = $first->graph_engine;
      $type = $first->graph_type;
      $saved_config = mycharts_object_to_array(json_decode($first->config));

      if ($first->field_presets) {
        $field_presets = json_decode($first->field_presets);

        if (!empty($field_presets)) {
          $label_field = $field_presets->label_field;
          $data_fields = $field_presets->data_fields;
        }
      }
    }

    $form = array();

    $engine = isset($form_state['values']['engine']) ?
        $form_state['values']['engine'] : $engine;
    $type = isset($form_state['values']['type']) ?
        $form_state['values']['type'] : $type;
    $label_field = isset($form_state['values']['label_field']) ?
        $form_state['values']['label_field'] : $label_field;
    $data_fields = isset($form_state['values']['data_fields']) ?
        $form_state['values']['data_fields'] : $data_fields;

    module_load_include('inc', 'mycharts', 'includes/mycharts.pages');
    $field_options = $this->display->handler->get_field_labels();
    if ($field_options) {
      $first_field = key($field_options);
      $form['fields']['#theme'] = 'mycharts_settings_fields';
      $form['fields']['label_field'] = array(
        '#type' => 'radios',
        '#title' => t('Label field'),
        '#options' => $field_options + array('' => t('No label field')),
        '#default_value' => isset($label_field) ? $label_field : $first_field,
        '#weight' => -10,
      );
      $form['fields']['data_fields'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Data fields'),
        '#options' => $field_options,
        '#default_value' => isset($data_fields) ?
        $data_fields :
        array_diff(array_keys($field_options), array($first_field)),
        '#weight' => -9,
      );
    }

    $wrapper_id = 'config-form-styled';
    $form['config_wrapper']
      = mycharts_form($engine, $type, $saved_config, $wrapper_id);

    $form['config_wrapper']['wrapper']['#type'] = 'container';
    $form['config_wrapper']['wrapper']['#attributes']
      = array(
        'id' => array($wrapper_id),
        'class' => array('pconfig', 'mycharts-choose-form'),
      );
  }

  /**
   * Generate a form for setting options.
   */
  public function options_submit(&$form, &$form_state) {
    // Primary config properties.
    $graph_engine = isset($form_state['values']['engine']) ?
      $form_state['values']['engine'] : -1;
    $graph_type = isset($form_state['values']['type']) ?
      $form_state['values']['type'] : -1;

    $label_field = isset($form_state['values']['label_field']) ?
      $form_state['values']['label_field'] : NULL;
    $data_fields = isset($form_state['values']['data_fields']) ?
      $form_state['values']['data_fields'] : NULL;

    $config = isset($form_state['values']['config']) ?
      $form_state['values']['config'] : array();
    $config = json_encode($config);

    // Presets to know data field and label fields.
    $field_presets = json_encode(array(
      'label_field' => $label_field,
      'data_fields' => $data_fields,
    ));

    $object_type = "view";
    $object_id = $this->view->vid;
    $object_title = "Presets for " . $object_type . " with id " . $object_id;
    mycharts_save_chart_preset($object_title, $object_type,
      $object_id, $graph_engine, $graph_type, $config, $field_presets);

    parent::options_submit($form, $form_state);
  }

  /**
   * Render the display in this style.
   *
   * I am not sure which way to use this plugin yet. 
   * Should we do a custom render here and:
   *  1. build an object that can use our generic theme('mycharts') call,
   *  or
   *  2. should we have a custom theme used just for views display of
   * a mycharts view?
   *
   * Currently we are trying out #2
   */
  public function render() {
    $render = parent::render();

    return $render;
  }
}
