<?php
/**
 * @file
 * Google Charts MyCharts Plugin.
 */

module_load_include('inc', 'mycharts', 'mycharts.basepluginclass');

// Ctools plugin quasi-global configuration array.
$plugin = array(
  'title' => t("GoogleCharts"),
  'description' => t('Use Googles Chart API for mycharts.'),
  'class' => 'MyChartsGooglecharts',
  'version' => '1.0',
);

class MyChartsGooglecharts extends MyChartsBase {
  public $chartname = "";
  public $target = "";

  /**
   * Construct function.
   */
  public function __construct($path_to_module, $chart_type) {
    $this->chartname = "googlecharts";

    parent::__construct($path_to_module, $chart_type);
  }

  /**
   * Function to actual make data array for chart engine.
   */
  public function setChartData() {
    // Need to cast all values to integer.
    foreach ($this->data_array['#rows'] as $num => &$arr) {
      $label = '';
      // If first element is string, extract it.
      if (!is_numeric($arr[0])) {
        $label = array_shift($arr);
      }

      $arr = array_map('intval', $arr);

      // If label defined, need to add it into array again.
      if ($label) {
        array_unshift($this->data_array['#rows'][$num], $label);
      }
    }

    // Put first row back to array.
    array_unshift($this->data_array['#rows'], $this->data_array['#labels']);

    $this->settings['data'] = $this->data_array['#rows'];
    $this->settings['visualization']
      = $this->mychartsGoogleVisualizationType();
  }

  /**
   * Function to define type that will be understanable for google engine.
   */
  protected function mychartsGoogleVisualizationType($renderable_type = "") {
    if (!$renderable_type) {
      $renderable_type = $this->chart_type;
    }
    $types = array(
      'linechart' => 'LineChart',
      'barchart' => 'BarChart',
      'areachart' => 'AreaChart',
      'bubblechart' => 'BubbleChart',
      'candlestickchart' => 'CandlestickChart',
      'columnchart' => 'ColumnChart',
      'combochart' => 'ComboChart',
      'geochart' => 'GeoChart',
      'piechart' => 'PieChart',
      'scatterchart' => 'ScatterChart',
      'steppedareachart' => 'SteppedAreaChart',
    );

    drupal_alter('mycharts_google_visualization_types', $types);
    return isset($types[$renderable_type]) ? $types[$renderable_type] : FALSE;
  }

  /**
   * Function to get google library definition javascript.
   */
  public function getPathToJs() {
    return array(
      'https://www.google.com/jsapi'
      => array('type' => 'external', 'weight' => 0));
  }

  /**
   * Function to load chart definition package.
   */
  public function getLoadJs() {
    switch ($this->chart_type) {
      case 'geochart':
        $retarray
          = array("google.load('visualization',"
            . " '1', {'packages': ['geochart']});");
        break;

      default:
        $retarray = array("google.load('visualization',"
          . " '1', {packages:['corechart']});");
    }

    return $retarray;
  }
}
