/**
 * @file
 * JavaScript integration between Google Charts and Drupal.
 */
(function ($) {
  Drupal.behaviors.mychartsGoogle = {};
  Drupal.behaviors.mychartsGoogle.attach = function(context, settings) {
    $('.mycharts-googlecharts').once('mycharts-googlecharts', function() {
        if ($(this).attr('data-chart')) {
            var config = $.parseJSON($(this).attr('data-chart'));

            var wrap = new google.visualization.ChartWrapper();
            wrap.setChartType(config.visualization);
            wrap.setDataTable(config.data);
            wrap.setOptions(config.options);

            wrap.draw(this);
        }
    });
  }
})(jQuery);
