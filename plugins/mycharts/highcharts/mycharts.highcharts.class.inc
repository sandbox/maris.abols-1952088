<?php
/**
 * @file
 * Highcharts MyCharts Plugin.
 */

module_load_include('inc', 'mycharts', 'mycharts.basepluginclass');

// Ctools plugin quasi-global configuration array.
$plugin = array(
  'title' => t("HighCharts"),
  'description' => t('Use Highcharts API for mycharts.'),
  'class' => 'MyChartsHighcharts',
  'version' => '1.0',
);

class MyChartsHighcharts extends MyChartsBase {
  public $chartname = "";

  /**
   * Construct function.
   */
  public function __construct($path_to_module, $chart_type) {
    $this->chartname = "highcharts";

    parent::__construct($path_to_module, $chart_type);
  }

  /**
   * Function to set chart title for y axis.
   */
  public function setYAxisTitle($title) {
    $this->settings['yAxis']['title']['text'] = $title;
  }

  /**
   * Function to set chart title for x axis.
   */
  public function setXAxisTitle($title) {
    $this->settings['xAxis']['categories'] = $title;
  }

  /**
   * Function to set DOM object id, where to put chart into document.
   */
  public function setChartWrapper($id) {
    $this->settings['chart']['renderTo'] = $id;
  }

  /**
   * Function to set all axis titles for charts.
   */
  public function setAxisTitle() {
    if (empty($this->data_array['#labels'])) {
      return;
    }

    $first_label = array_shift($this->data_array['#labels']);

    // Set y axis title.
    $this->setYAxisTitle($first_label);

    // Set x axis titles.
    $this->setXAxisTitle($this->data_array['#labels']);
  }

  /**
   * Function to actual make data array for chart engine.
   */
  public function setChartData() {
    // Titles.
    $this->setAxisTitle();

    foreach ($this->data_array['#rows'] as $num => $arr) {
      $label = "";
      if (!is_numeric($arr[0])) {
        $label = array_shift($arr);
      }
      $arr = array_map('intval', $arr);

      $this->settings['series'][$num]['name'] = $label;
      $this->settings['series'][$num]['data'] = $arr;
    }
  }

  /**
   * Function to get highcharts library definition javascript.
   */
  public function getPathToJs() {
    $library_path = libraries_get_path($this->chartname);
    return array(
      $library_path . '/js/highcharts.js'
      => array('type' => 'file', 'weight' => 0));
  }
}
