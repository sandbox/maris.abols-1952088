/**
 * @file
 * JavaScript integration between Highcharts and Drupal.
 */
(function ($) {
  Drupal.behaviors.mychartsHighcharts = {};
  Drupal.behaviors.mychartsHighcharts.attach = function(context, settings) {
    $('.mycharts-highcharts').once('mycharts-highcharts', function() {
      if ($(this).attr('data-chart')) {
        var config = $.parseJSON($(this).attr('data-chart'));
        $(this).highcharts(config);
      }
    });
  };
})(jQuery);
