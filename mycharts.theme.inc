<?php
/**
 * @file
 * Themeing implementation functions for mycharts.
 * @note there is 1 theme handler in the mycharts.views.inc
 */

/**
 * Heavy mycharts chart theme function.
 *
 * It pulls a lot of the functionality directly from
 * the original form build function.  This will require
 * refactoring in order to be useful.
 */
function theme_mychart($element) {
  module_load_include('inc', 'mycharts', 'mycharts');
  module_load_include('inc', 'mycharts', 'mycharts.helper');

  $path = drupal_get_path('module', 'mycharts');
  $element['#attached']['css'][] = $path . '/css/mycharts.css';

  // $id can come from either the element, the settings or the default.
  if (!empty($element['#id'])) {
    // First check a #id from the element.
    $element_id = $element['#id'];
  }
  else {
    // Default to an auto-generated id.
    $element_id = drupal_html_id('element');
  }

  // Maybe a chart object was given to us.
  if (isset($element['#chart'])) {
    $chart = $element['#chart'];
  }
  else {
    // We have to build the chart object.
    // If no chart object was given, then the following are required:
    // $settings['engine'] : (string) the graphing engine
    // $settings['type'] : (string) the chart type
    // $element['#data'] : the chart data array, one element per
    // seried, with optionally labels as the first element.
    // Load chart type options.
    $data = $element['#data'];
    $settings = $element['#settings'];
    $engine = $element['#engine'];
    $type = $element['#type'];
    $chart = mycharts_buildobject($engine, $type, $settings, $data);
  }

  $element_type
    = isset($element['#element_type']) ? $element['#element_type'] : 'div';
  // Here you could pass in additional.
  $element_attributes
    = isset($element['#attributes']) ? $element['#attributes'] : array();
  // Force the id into the attribute array.
  $element_attributes['id'] = $element_id;

  // Make sure that we have some css/js target options @TODO expand on this.
  if (!isset($element_attributes['class'])) {
    $element_attributes['class'] = array();
    $element_attributes['class'][] = 'mycharts-element';
  }

  if ($chart) {
    drupal_add_library('mycharts', "chart_" . $chart->chartname);
    // Already fixed data.
    $element_attributes['data-chart']
      = drupal_json_encode($chart->getSettings());

    if (isset($element_attributes['class'])) {
      $element_attributes['class'][] = 'mycharts-' . $chart->chartname;
    }
  }

  // @TODO USE THIS NAMESPACE INSTEAD, IT IS MUCH MORE DRUPAL FRIENDLY :
  // drupal_add_js('mycharts'=>array($element_id=>$settings),
  // array('type' => 'setting', 'cache' => FALSE));
  return '<' . $element_type . ' ' . drupal_attributes($element_attributes)
      . '>' . t('Loading chart..') . '</' . $element_type . '>';
}
