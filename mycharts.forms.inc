<?php
/**
 * @file
 * File to define config forms for mycharts.
 */

// @TODO These should be in a MYCHARTS namesspace
/* define('CONFIG_NEW_ELEM_POSTFIX', "-addnew-multiple");
 define('CONFIG_SELECTBOX_CONTAINER_POSTFIX', '-multiple-selectbox-container');
 */

/**
 * Function to retrieve chart config form.
 */
function mycharts_chart_details_form($form, &$form_state, $node) {
  module_load_include('inc', 'mycharts', 'mycharts.helper');

  $chart_engine = "";
  $chart_type = "";
  $saved_config = "";

  $type = 'node';

  $chart_data = array();
  if ($type == 'node') {
    // Load saved data for current object.
    $presets = mycharts_load_chart_preset('node', $node->nid);

    foreach ($node as $field => $value) {
      $field_info = field_info_field($field);

      if (!empty($field_info) && $field_info['type'] == 'tablefield') {
        $chart_data_field_items = field_get_items('node', $node, $field);

        if ($chart_data_field_items) {
          $chart_data_field_items = reset($chart_data_field_items);

          $chart_data = $chart_data_field_items['tabledata'];
        }

        break;
      }
    }
  }

  if (isset($presets) && !empty($presets)) {
    $first = array_shift($presets);

    $chart_engine = $first->graph_engine;
    $chart_type = $first->graph_type;
    $saved_config = mycharts_object_to_array(json_decode($first->config));
  }

  // Load chart type options.
  $chart_engine = isset($form_state['input']['engine']) ? $form_state['input']['engine'] : $chart_engine;
  $chart_type = isset($form_state['input']['type']) ? $form_state['input']['type'] : $chart_type;

  $saved_config = isset($form_state['input']['config']) ? $form_state['input']['config'] : $saved_config;

  $wrapper_id = drupal_html_id('config-form');

  $form = mycharts_form($chart_engine, $chart_type, $saved_config, $wrapper_id);
  $form['object_id'] = array(
    '#type' => 'hidden',
    '#value' => $node->nid,
  );

  $form['object_type'] = array(
    '#type' => 'hidden',
    '#value' => 'node',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#attributes' => array('alt' => t('Save')),
  );

  $form['#attached'] = array(
    'library' => array(
      array('system', 'farbtastic'),
    ),
    'js' => array(
      drupal_get_path('module', 'mycharts') . '/js/colorpicker.js',
    ),
  );

  // Tablefield's first row have labels.
  $first_row = array_shift($chart_data);

  $data = array(
    '#labels' => $first_row,
    '#rows' => $chart_data,
  );

  // Chart preview demo.
  $element = array(
    '#data' => $data,
    '#id' => drupal_html_id('graph-preview'),
    '#settings' => $saved_config,
    '#engine' => $chart_engine,
    '#type' => $chart_type,
  );

  $form['interactive']['chart_preview'] = array(
    '#type' => 'item',
    '#prefix' => theme('mychart', $element),
  );

  $colorpicker_path = libraries_get_path("colorpicker");
  if ($colorpicker_path) {
    $form['#attached']['css'][] = $colorpicker_path . '/css/colorpicker.css';
    $form['#attached']['js'][] = $colorpicker_path . '/js/colorpicker.js';
  }

  $mycharts_path = drupal_get_path('module', 'mycharts');
  $form['#attached']['css'][] = $mycharts_path . '/css/mycharts.css';

  return $form;
}

/**
 * Function to form submit callback - do save graph presets into db.
 */
function mycharts_chart_details_form_submit($form, $form_state) {
  module_load_include('inc', 'mycharts', 'mycharts.helper');

  // Primary config properties.
  $graph_engine = isset($form_state['values']['engine']) ? $form_state['values']['engine'] : -1;
  $graph_type = isset($form_state['values']['type']) ? $form_state['values']['type'] : -1;
  $config = isset($form_state['values']['config']) ? $form_state['values']['config'] : array();
  $object_id = isset($form_state['values']['object_id']) ? $form_state['values']['object_id'] : -1;
  $object_type = isset($form_state['values']['object_type']) ? $form_state['values']['object_type'] : -1;

  $config = json_encode($config);

  $object_title = "Presets for " . $object_type . " with id " . $object_id;
  mycharts_save_chart_preset($object_title, $object_type, $object_id, $graph_engine, $graph_type, $config);

  drupal_set_message(t('Chart config saved.'));
}

/**
 * Function to get config form for mycharts with engine and chart type fields.
 */
function mycharts_form($engine = '', $chart = '', $config = array(), $wrapper_id = '') {
  module_load_include('inc', 'mycharts', 'mycharts.plugins');

  $engine_config = mycharts_engine_getConfig($engine);
  $type_config = mycharts_chart_getConfig($engine, $chart);

  $element['interactive'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('mycharts-choose-form'), 'id' => array($wrapper_id)),
  );

  $element['interactive']['choose'] = mycharts_form_choose($engine, $chart);
  $element['interactive']['config'] = mycharts_form_config($config, $engine_config, $type_config);

  $element['#attached']['css'][] = drupal_get_path('module', 'mycharts') . '/css/mycharts.css';

  return $element;
}

/**
 * Function to return form fields for chart engine and type.
 */
function mycharts_form_choose($engine = '', $chart = '') {
  $element['engine'] = array(
    '#type' => 'select',
    '#title' => t('Chart engine'),
    '#options' => mycharts_engines_options(),
    '#default_value' => $engine,
    '#prefix' => '<div class="chart-config-box fields-config-box">',
    '#attributes' => array('id' => array('engine-wrapper')),
  );

  $element['type'] = array(
    '#type' => 'select',
    '#title' => t('Chart type'),
    '#options' => _mycharts_chart_options($engine),
    '#default_value' => $chart,
    '#prefix' => '<div id="chart-type-wrapper">',
    '#suffix' => '</div></div>',
    '#attributes' => array('id' => array('type-wrapper')),
  );

  return $element;
}

/**
 * Settings form for an individual chart engine/type.
 */
function mycharts_form_config($config, $engine_config, $chart_config, $series_count = NULL, $parent = '') {
  // Outer form element for our config.
  return array_merge(array(
    '#type' => 'container',
    '#tree' => TRUE,
  ), _mycharts_form_config($config, $engine_config, $chart_config, $series_count = NULL, $parent = ''));
}

/**
 * Function render actual config form.
 */
function _mycharts_form_config($config, $engine_config, $chart_config, $series_count = NULL, $parent = '') {
  module_load_include('inc', 'mycharts', 'mycharts.helper');

  $element = array();

  if (!empty($chart_config)) {
    // Used to keep sorting order for form elements.
    $index = 0;
    foreach ($chart_config as $key => $value) {
      $setting_name = empty($parent) ? $key : $parent . '-' . $key;
      // Try to get a settings value from our existing settings.
      $config_value = isset($config[$key]) ? $config[$key] : NULL;

      // LOCKED values can't be edited.
      if (isset($engine_config['locked']) && isset($engine_config['locked'][$setting_name])) {
        $element[$key] = array(
          '#type' => 'item',
          '#title' => ucfirst($key),
          '#markup' => !is_null($config_value) ? $config_value : $value,
          '#prefix' => '<div class="float-left">',
          '#suffix' => '</div>',
        );
        continue;
      }

      switch (gettype($value)) {
        // Easy cases first.
        case 'integer':
        case 'double':
          // @TODO - need to add some validation for numeric type fields.
          $element[$key] = array(
            '#type' => 'textfield',
            '#title' => ucfirst($key),
            '#default_value' => !is_null($config_value) ? $config_value : $value,
            '#value' => !is_null($config_value) ? $config_value : $value,
            '#size' => 10,
            '#maxlength' => 10,
            '#prefix' => '<div class="float-left">',
            '#suffix' => '</div>',
          );
          break;

        case 'string':
          $element[$key] = array(
            '#type' => 'textfield',
            '#title' => ucfirst($key),
            '#default_value' => !is_null($config_value) ? $config_value : $value,
            '#value' => !is_null($config_value) ? $config_value : $value,
            '#size' => 60,
            '#maxlength' => 128,
            '#prefix' => '<div class="float-left">',
            '#suffix' => '</div>',
          );

          // Colorpicker for color type fields.
          if (strpos(strtolower($key), "color") !== FALSE) {
            $element[$key]['#attributes'] = array(
              'class' => array('simple-colorpicker'),
            );
          }

          break;

        // I know my indenting here is bad, but I am using it to separate the
        // child options from the multiple value settings.
        case 'array':
          if (!_mycharts_form_settings_setting_isChildSettings($value)) {
            // This is a multiple value field.
            $is_boolean_values = mycharts_is_only_boolean_labels($value);

            // Refactor selectbox options array.
            if (!$is_boolean_values) {
              $value = mycharts_build_config_select_values($value);
            }

            // @TODO - need to define into config default value somehow.
            $element[$key] = array(
              '#type' => 'select',
              '#title' => ucfirst($key),
              '#options' => $value,
              '#default_value' => $config_value,
              '#value' => $config_value,
              '#prefix' => '<div class="float-left ' . $setting_name . '-wrapper">',
              '#suffix' => '</div>',
            );

            // Consider Multiple value fields.
            // @TODO
          }
          else {
            // This is a set of children fields.
            $label = $key;
            if (is_numeric($key)) {
              $label = '[' . $key . ']';
            }

            $element[$key] = array(
              '#type' => 'fieldset',
              '#tree' => TRUE,
              '#title' => $label,
              '#weight' => $index,
              '#collapsible' => TRUE,
              '#collapsed' => TRUE,
            );

            // SPECIAL element - series, who provide configure each of
            // chart serie.
            if (isset($engine_config['series']) &&
                isset($engine_config['series'][$setting_name])) {
              $ret_array = $value;
              for ($x = 0; $x < $series_count - 1; $x++) {
                $tmp_array = array_merge(array(), $value);
                $ret_array = array_merge($ret_array, $tmp_array);
              }

              $value = $ret_array;
            }

            // Parse sub elements.
            if (!isset($config[$key])) {
              $config[$key] = array();
            }

            $element[$key] += mycharts_form_config($config[$key], $engine_config, $value, $series_count, $setting_name);
          }
      }

      // Colorpicker for color type fields.
      if (strpos(strtolower($setting_name), "color") !== FALSE) {
        $element[$key]['#attributes'] = array(
          'class' => array('simple-colorpicker'),
        );
      }

      $index++;
    }
  }

  return $element;
}

/**
 * Function to determine config setting type.
 *
 * Because or our simple yml file concept, it is hard to differentiate between
 * a set of child options, or a multiple value single option.
 *
 * Basically what we do is we test to see if both:
 *  - array keys are numeric.
 *  - array values are not arrays or objects.
 */
function _mycharts_form_settings_setting_isChildSettings($value) {
  return (!(
    _mycharts_check_array_keys_type($value, array('integer', 'double'))
    && mycharts_check_array_values_type($value,
        array('integer', 'double', 'boolean', 'string'))
  ));
}

/**
 * Function to check if array keys are all numeric.
 */
function _mycharts_check_array_keys_type($arr, $types = array()) {
  if (!empty($arr)) {
    $keys = array_keys($arr);

    foreach ($keys as $k) {
      $type = gettype($k);

      if (!in_array($type, $types)) {
        return FALSE;
      }
    }
  }

  return TRUE;
}

/**
 * Function to check if array values are all string(or something else).
 */
function mycharts_check_array_values_type($arr, $types = array('string')) {
  if (!empty($arr)) {
    foreach ($arr as $v) {
      if (!in_array(gettype($v), $types)) {
        return FALSE;
      }
    }
  }
  return TRUE;
}

/**
 * Function to load chart engines dropdown elements.
 */
function mycharts_engines_options() {
  module_load_include('inc', 'mycharts', 'mycharts.plugins');

  $chart_engines = mycharts_engine_list();

  // Refactor engine options.
  $chart_engines_options = array('' => t('Choose one'));
  if (!empty($chart_engines)) {
    foreach ($chart_engines as $name => $plugin) {
      $chart_engines_options[$name] = $plugin['title'];
    }
  }

  return $chart_engines_options;
}

/**
 * Function to load chart type dropdown elements.
 */
function _mycharts_chart_options($engine = NULL) {
  module_load_include('inc', 'mycharts', 'mycharts.plugins');

  // Refactor chart type options.
  if ($engine) {
    $chart_type_options = array('' => t('Choose one'));
    $chart_types = mycharts_chart_list($engine);
    if (!empty($chart_types)) {
      foreach ($chart_types as $name => $file) {
        // Transfer to human readable format (ar at least half human).
        $name = str_replace("_", " ", $name);
        $chart_type_options[$name] = ucfirst($name);
      }
    }
  }
  else {
    $chart_type_options = array('' => t('Choose an engine'));
  }

  return $chart_type_options;
}
